Ressources pour utiliser BlocksCAD au cycle 3 en vue d'imprimer des objets en 3D.

![Logo BlocksCAD](images/blocksCAD_logo_carré.svg)

Page du projet : [https://forge.apps.education.fr/thierrym/blockscad](https://forge.apps.education.fr/thierrym/blockscad)

Page publique du projet : [https://thierrym.forge.apps.education.fr/blockscad](https://thierrym.forge.apps.education.fr/blockscad)

## Présentation
Dans ce dépôt public, vous trouverez des ressources modifiables (au format .odg) mais aussi au format .pdf permettant aux élèves et enseignant⋅es de cycle 3 de s'approprier le logiciel BlocksCAD en vue de réaliser des objets à imprimer en 3D. Il y 4 fiches d'exercices aux difficultés progressives avec correction.
Vous y trouverez aussi des projets complets pour réaliser des objets à l'imprimante 3D.
Le marque-page est plus abordable que le pendentif pour des élèves de cycle 3.

## Contenus
- Fiches progressives d'activités sur BlocksCAD au format modifiable .odg : [Fiches_BlocksCAD.odg](Fiches_BlocksCAD.odg)
- Mêmes fiches que ci-dessus au format .pdf : [Fiches_BlocksCAD.pdf](Fiches_BlocksCAD.pdf)
- Dossier contenant des documents explicitant le process de l'impression 3D (présentation .odp, .pdf mais aussi .drawio): [Impression 3D](https://forge.apps.education.fr/thierrym/blockscad/-/tree/main/Impression%203D)
- Dossier contenant des documents pour fabriquer un marque-page (fichiers .odt, .pdf et Geogebra .ggb) : [Marque-page](https://forge.apps.education.fr/thierrym/blockscad/-/tree/main/Marque-page)
- Dossier contenant des documents pour fabriquer un pendentif (fichiers .odt, .pdf et Geogebra .ggb) : [Pendentif](https://forge.apps.education.fr/thierrym/blockscad/-/tree/main/Pendentif)

## Installation de BlocksCAD
L'avantage de BlocksCAD est de pouvoir l'utiliser en version desktop, ne demandant pas de connexion Internet. Il faut récupérer l'archive ici [https://github.com/EinsteinsWorkshop/BlocksCAD/archive/refs/heads/development.zip](https://github.com/EinsteinsWorkshop/BlocksCAD/archive/refs/heads/development.zip), la décompresser et lancer le fichier "index.html" qui se lance dans un navigateur Internet.

## Licence
Les ressources sont sous licence Creative Commons 4.0 : Paternité - Pas d'Utilisation Commerciale-Partage des Conditions Initiales à l'Identique [https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)
